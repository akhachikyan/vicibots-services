import time as t

from services import scheduler
from services import _storage

SERVICE_NAME = "todo_list"


# Reschedule events.
def _reschedule_events():
    # Load service.
    loaded_settings = _storage.load_all(SERVICE_NAME)

    # Loop to load
    for user_id, settings in loaded_settings.iteritems():
        for item in settings:
            event_time, note = item

            # Reschedule event if in future.
            if event_time > t.time():
                scheduler.add_event(SERVICE_NAME, user_id, event_time, note)

# Need to call this once.
_reschedule_events()


# Adds item.
def add(user_id, note, event_time=t.time()):
    if note:
        settings = _storage.load(SERVICE_NAME, user_id)

        # Add new data.
        new_data = (event_time, note)
        settings.append(new_data)

        # Save new settings.
        _storage.store(SERVICE_NAME, user_id, settings)

        # Schedule event if in future.
        if event_time > t.time():
            scheduler.add_event(SERVICE_NAME, user_id, event_time, note)
    else:
        raise ValueError("Invalid arguments!", note)


# Returns list of items.
def list_items(user_id):
    return _storage.load(SERVICE_NAME, user_id)


# Remove item for given index.
def remove(user_id, index):
    settings = _storage.load(SERVICE_NAME, user_id)
    if 0 <= index < len(settings):
        event_time, __ = settings.pop(index)

        # Cancel event.
        scheduler.cancel_event(SERVICE_NAME, user_id, event_time)

        # Save result.
        _storage.store(SERVICE_NAME, user_id, settings)
    else:
        raise ValueError("Invalid index!", index)
