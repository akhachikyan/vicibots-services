from services import _storage

# Sample usages.
_storage.store("alarm", 123, ["Set Alarm 10 AM", "Another Alarm \n V:P"])

data = _storage.load("alarm", 123)
print data

data = _storage.load("alarm", 124)
print data
