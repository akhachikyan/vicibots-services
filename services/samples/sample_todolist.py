import time

from services import scheduler
from services import todolist

todolist.add(78, "No idea what to do.")
todolist.add(78, "Do some work!", time.time() + 1)
todolist.add(78, "Sleeping Zzz", time.time() + 6)
todolist.add(78, "Having some food.", time.time() + 10)

todolist.remove(78, 0)

items = todolist.list_items(78)
print items


# Listener for todo_list.
def todo_list_listener(user_id, event_data):
    print "EVENT: {user} - {event_data}".format(user=user_id, event_data=event_data)

# Attach event listener for todo_list.
scheduler.attach(todolist.SERVICE_NAME, todo_list_listener)

# Wait for scheduler to do his work.
time.sleep(60)
