import threading
import time as t

import schedule

from services import _config

# Event listeners.
listeners = {}

# Scheduled events.
events = {}


# Notify all service listeners about event.
def _on_event(service, user_id, event_data):
    if service in listeners:
        service_listeners = listeners[service]
        if service_listeners:
            # Invoke listeners for given service.
            for listener in service_listeners:
                listener(user_id, event_data)


# Loop method for scheduler.
def _do_loop():
    # Use these to swap values.
    global events
    active_events = {}

    # Current time.
    now = t.time()

    # Loop all events to extract valid events.
    for key, event_data in events.iteritems():
        service, user_id, event_time = key.split("-")

        # Check if event is in current loop.
        if float(event_time) <= now:
            _on_event(service, user_id, event_data)
        else:
            active_events[key] = event_data

    # Reset event data with only active events.
    events = active_events


# We need this to start scheduler in separate thread.
def _loop_thread():
    # Start scheduler.
    schedule.every(_config.LOOP_DURATION).seconds.do(_do_loop)

    while 1:
        schedule.run_pending()
        t.sleep(1)

# Start loop.
worker_thread = threading.Thread(target=_loop_thread)
worker_thread.start()


# Attach service listener.
def attach(service, listener):
    # Check if there are listeners.
    if service in listeners:
        service_listeners = listeners[service]
        service_listeners.append(listener)
    else:
        listeners[service] = [listener]


# Detach service listener.
def detach(service, listener):
    if service in listeners:
        service_listeners = listeners[service]
        service_listeners.discard(listener)


# Add event.
def add_event(service, user_id, event_time, params):
    key = "{service}-{user}-{t}".format(service=service, user=user_id, t=event_time)
    events[key] = event_time, params


# Cancel event.
def cancel_event(service, user_id, event_time):
    key = "{service}-{user}-{t}".format(service=service, user=user_id, t=event_time)
    if key in events:
        events.pop(key)
