import json
import os

from services import _config


# Service directory path.
def _dir_path(service):
    # Prepare directory for storage.
    dir_path = '{data}/{service}'.format(data=_config.STORAGE_DIR, service=service)

    # Make sure it exists.
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    return dir_path


# Returns file path for user and given service.
def _file_path(service, user_id):
    dir_path = _dir_path(service)

    file_name = "{user}".format(user=user_id)

    # Return path.
    return os.path.join(dir_path, file_name)


# Stores user's settings for given service.
def store(service, user_id, settings):
    if settings and user_id:
        file_path = _file_path(service, user_id)

        # If single item, wrap it with list.
        if not isinstance(settings, list):
            settings = [settings]

        # Open file to write.
        with open(file_path, 'w+') as f:
            # Write data in json format.
            settings_json = json.dumps(settings, indent=2)
            f.write(settings_json)
    else:
        raise ValueError("Invalid arguments!", settings, user_id)


# Loading user's settings for given service.
def load(service, user_id):
    if user_id:
        file_path = _file_path(service, user_id)
        file_exists = os.path.isfile(file_path)

        if file_exists:
            # Open file to read.
            with open(file_path) as f:
                settings_json = f.read()
                return json.loads(settings_json)
        else:
            return []
    else:
        raise ValueError("Invalid arguments!", user_id)


# Loading all data for service.
def load_all(service):
    settings = {}
    dir_path = _dir_path(service)

    # Search for users.
    for user_id in os.listdir(dir_path):
        settings[user_id] = load(service, user_id)

    # Return result.
    return settings
