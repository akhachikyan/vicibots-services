#!/usr/bin/env python

from setuptools import setup, find_packages
from pip.req import parse_requirements
from pip.download import PipSession

# parse_requirements() returns generator of pip.req.InstallRequirement objects
install_requirements = parse_requirements('requirements.txt', session=PipSession())

# requirements is a list of requirement
requirements = [str(ir.req) for ir in install_requirements]

setup(name='viciservices',
      version='1.0',
      description='Services for Vicibots.',
      author='Gevorg Harutyunyan',
      author_email='gevorg.ha@gmail.com',
      url='https://bitbucket.org/gevorg/vicibots-services/',
      packages=find_packages(),
      install_requires=requirements
      )
